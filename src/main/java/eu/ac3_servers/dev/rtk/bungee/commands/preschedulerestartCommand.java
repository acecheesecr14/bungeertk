package eu.ac3_servers.dev.rtk.bungee.commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import eu.ac3_servers.dev.rtk.bungee.RTKCommand;
import eu.ac3_servers.dev.rtk.bungee.RTKPlugin;
import eu.ac3_servers.dev.rtk.bungee.UDPPacketSender;

public class preschedulerestartCommand extends RTKCommand {

	public preschedulerestartCommand(RTKPlugin plugin) {
		super(plugin, "preschedulerestart");
		// TODO Auto-generated constructor stub
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender commandsender, String[] as) {

		if(!(commandsender instanceof ProxiedPlayer)){
			commandsender.sendMessage(ChatColor.RED + "Only players can do this command!");
			return;
		}
		
		ProxiedPlayer player2 = (ProxiedPlayer) commandsender;
		if (as.length < 2) {
			player2.sendMessage((new StringBuilder())
					.append(ChatColor.RED)
					.append("Usage: /preschedulerestart <[username:]password> <time>")
					.toString());
			return;
		}
		String s4 = "";
		for (int l = 1; l < as.length; l++)
			s4 = (new StringBuilder()).append(s4).append(as[l].trim())
					.append(" ").toString();

		s4 = s4.replaceAll(":", "-");
		if (as[0].contains(":"))
			plugin.getProxy().getScheduler().runAsync(plugin, new UDPPacketSender(
					(new StringBuilder()).append("reschedule:")
							.append(s4.trim()).append(":")
							.append(as[0].trim()).toString(), player2, plugin, port));
		else
			plugin.getProxy().getScheduler().runAsync(plugin, new UDPPacketSender(
					(new StringBuilder()).append("reschedule:")
							.append(s4.trim()).append(":")
							.append(player2.getName()).append(":")
							.append(as[0].trim()).toString(), player2, plugin, port));
		
	}

}
