package eu.ac3_servers.dev.rtk.bungee.commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import eu.ac3_servers.dev.rtk.bungee.RTKCommand;
import eu.ac3_servers.dev.rtk.bungee.RTKPlugin;
import eu.ac3_servers.dev.rtk.bungee.UDPPacketSender;

@SuppressWarnings("deprecation")
public class restartproxyCommand extends RTKCommand {

	public restartproxyCommand(RTKPlugin plugin) {
		super(plugin, "restartproxy");
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void execute(CommandSender commandsender, String[] as) {
		
		if(!(commandsender instanceof ProxiedPlayer)){
			commandsender.sendMessage(ChatColor.RED + "Only players can do this command!");
			return;
		}
		
		ProxiedPlayer player1 = (ProxiedPlayer) commandsender;
		if (as.length != 1) {
			player1.sendMessage((new StringBuilder())
					.append(ChatColor.RED)
					.append("Usage: /restartsrv <[username:]password>")
					.toString());
			return;
		}
		if (as[0].contains(":"))
			plugin.getProxy().getScheduler().runAsync(plugin, new UDPPacketSender(
					(new StringBuilder()).append("restart:")
							.append(as[0].trim()).toString(), player1, plugin, port));
		else
			plugin.getProxy().getScheduler().runAsync(plugin, new UDPPacketSender(
					(new StringBuilder()).append("restart:")
							.append(player1.getName()).append(":")
							.append(as[0].trim()).toString(), player1, plugin, port));
		return;
	}

}
