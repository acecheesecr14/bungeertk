package eu.ac3_servers.dev.rtk.bungee.commands;

import eu.ac3_servers.dev.rtk.bungee.RTKCommand;
import eu.ac3_servers.dev.rtk.bungee.RTKPlugin;
import eu.ac3_servers.dev.rtk.bungee.UDPPacketSender;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class pstopwrapperCommand extends RTKCommand {

	public pstopwrapperCommand(RTKPlugin plugin) {
		super(plugin, "pstopwrapper");
		// TODO Auto-generated constructor stub
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender commandsender, String[] as) {
		
		if(!(commandsender instanceof ProxiedPlayer)){
			commandsender.sendMessage(ChatColor.RED + "Only players can do this command!");
			return;
		}
		
		ProxiedPlayer player3 = (ProxiedPlayer) commandsender;
		if (as.length != 1) {
			player3.sendMessage((new StringBuilder())
					.append(ChatColor.RED)
					.append("Usage: /stopwrapper <[username:]password>")
					.toString());
			return;
		}
		if (as[0].contains(":"))
			plugin.getProxy().getScheduler().runAsync(plugin, new UDPPacketSender(
					(new StringBuilder()).append("stopwrapper:")
							.append(as[0].trim()).toString(), player3, plugin, port));
		else
			plugin.getProxy().getScheduler().runAsync(plugin, new UDPPacketSender((new StringBuilder()).append("stopwrapper:")
							.append(player3.getName()).append(":")
							.append(as[0].trim()).toString(), player3, plugin, port));
		return;
		
	}

}
