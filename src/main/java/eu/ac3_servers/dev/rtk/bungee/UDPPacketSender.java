package eu.ac3_servers.dev.rtk.bungee;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.util.logging.Level;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class UDPPacketSender implements Runnable {

	private String s;
	private ProxiedPlayer player;
	private RTKPlugin plugin;
	private int port;

	private final String denyStrings[] = { 
			"No.",
			"Make me.",
			"Nice try!",
			"It works if you know the password.",
			"You're not being very persuasive.",
			"You didn't say the magic word!",
			"No! You're not my mother.",
			"Once again, with feeling!",
			"The password gods have frowned upon you.",
			"My mother told me not to talk to strangers.",
			};

	public UDPPacketSender(String s, final ProxiedPlayer player, RTKPlugin plugin,
			int port) {

		this.s = s;
		this.player = player;
		this.plugin = plugin;
		this.port = port;

	}

	@SuppressWarnings({ "deprecation", "static-access" })
	@Override
	public void run() {

		try {
			DatagramSocket ds = new DatagramSocket();

			DatagramPacket datagrampacket = new DatagramPacket(s.getBytes(),
					s.getBytes().length, InetAddress.getByName("localhost"),
					port);
			ds.send(datagrampacket);

			byte abyte0[] = new byte[65536];
			DatagramPacket datagrampacket1 = new DatagramPacket(abyte0,
					abyte0.length);
			
			try {
				ds.setSoTimeout(700);
				ds.receive(datagrampacket1);
				String s1 = new String(datagrampacket1.getData());
				if (s1.trim().equalsIgnoreCase("response:success"))
					player.sendMessage((new StringBuilder())
							.append(ChatColor.DARK_BLUE).append("[PROXY] ")
							.append(ChatColor.RED).append("Done!").toString());
				else if (s1.trim().equalsIgnoreCase("response:command_error"))
					player.sendMessage((new StringBuilder())
							.append(ChatColor.DARK_BLUE).append("[PROXY] ")
							.append(ChatColor.RED)
							.append("Invalid restart time!").toString());
				else
					player.sendMessage((new StringBuilder())
							.append(ChatColor.DARK_BLUE).append("[PROXY] ")
							.append(ChatColor.RED)
							.append(denyStrings[(int) (Math.random() * (double) denyStrings.length)])
							.toString());
			} catch (SocketTimeoutException sockettimeoutexception) {
				ds.close();
				player.sendMessage((new StringBuilder())
						.append(ChatColor.DARK_BLUE).append("[PROXY] ")
						.append(ChatColor.RED)
						.append(denyStrings[(int) (Math.random() * (double) denyStrings.length)])
						.toString());
			} catch (Exception exception1) {
				ds.close();
				plugin.log.log(Level.INFO, "Unexpected Socket error: {0}",
						exception1);
				exception1.printStackTrace();
			}

			ds.close();
		} catch (IOException e) {
			e.printStackTrace();
			plugin.log.log(Level.INFO,
					"Error in Remote Toolkit plugin: {0}", e);
		}

	}

}
