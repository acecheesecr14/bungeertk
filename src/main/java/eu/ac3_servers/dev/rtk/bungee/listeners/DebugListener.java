package eu.ac3_servers.dev.rtk.bungee.listeners;

import eu.ac3_servers.dev.rtk.bungee.RTKPlugin;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

@SuppressWarnings("static-access")
public class DebugListener implements Listener {
	
	private RTKPlugin plugin;

	
	public DebugListener(RTKPlugin plugin) {
		
		this.plugin = plugin;
		
		this.plugin.log.info("Debug listener registered!");
		
	}
	
	@EventHandler
	public void onChat(ChatEvent e){
		
		if(e.getMessage().startsWith("/")){
			
			this.plugin.log.info((new StringBuilder(e.getSender().getAddress().getAddress().toString()).append(": ").append(e.getMessage()).toString()));
			
		}
		
	}

}
