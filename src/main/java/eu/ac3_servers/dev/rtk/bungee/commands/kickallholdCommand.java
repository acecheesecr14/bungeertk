package eu.ac3_servers.dev.rtk.bungee.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import eu.ac3_servers.dev.rtk.bungee.RTKCommand;
import eu.ac3_servers.dev.rtk.bungee.RTKPlugin;

public class kickallholdCommand extends RTKCommand {

	public kickallholdCommand(RTKPlugin plugin) {
		super(plugin, "kickallhold");
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender commandsender, String[] as) {

		super.execute(commandsender, as);
		
		String s2 = ((String) plugin.getMessageMap().get(
				"hold-kick-message")).replaceAll(new String(new byte[] {
				92, 92, 110 }), "\n");
		if (s2 == null)
			s2 = "Server is shutting down";
		for (ProxiedPlayer player : plugin.getProxy().getPlayers()) {
			player.disconnect(s2);
		}
		
	}

}
