package eu.ac3_servers.dev.rtk.bungee.commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import eu.ac3_servers.dev.rtk.bungee.RTKCommand;
import eu.ac3_servers.dev.rtk.bungee.RTKPlugin;
import eu.ac3_servers.dev.rtk.bungee.UDPPacketSender;

public class holdproxyCommand extends RTKCommand {

	public holdproxyCommand(RTKPlugin plugin) {
		super(plugin, "holdproxy");
		
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender commandsender, String[] as) {

		if(!(commandsender instanceof ProxiedPlayer)){
			commandsender.sendMessage(ChatColor.RED + "Only players can do this command!");
			return;
		}
		
		ProxiedPlayer player = (ProxiedPlayer) commandsender;
		if (as.length != 1) {
			player.sendMessage((new StringBuilder())
					.append(ChatColor.RED)
					.append("Usage: /holdsrv <[username:]password>")
					.toString());
			return;
		}
		if (as[0].contains(":"))
			plugin.getProxy().getScheduler().runAsync(plugin, new UDPPacketSender((new StringBuilder())
					.append("hold:").append(as[0].trim()).toString(),
					player, plugin, port));
		else
			plugin.getProxy().getScheduler().runAsync(plugin, new UDPPacketSender(
					(new StringBuilder()).append("hold:")
							.append(player.getName()).append(":")
							.append(as[0].trim()).toString(), player, plugin, port));
		
	}

}
