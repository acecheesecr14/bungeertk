package eu.ac3_servers.dev.rtk.bungee.listeners;

import java.util.logging.Level;

import eu.ac3_servers.dev.rtk.bungee.RTKPlugin;
import eu.ac3_servers.dev.rtk.bungee.UDPPacketSender;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ChatListener implements Listener {

	private RTKPlugin plugin;
	private int port;
	private String startChar;

	@SuppressWarnings("static-access")
	public ChatListener(RTKPlugin plugin) {
		this.plugin = plugin;

		try {
			port = Integer.parseInt(((String) plugin.getProperties().get(
					"remote-control-port")).trim());
		} catch (Exception exception) {
			RTKPlugin.log.log(Level.INFO,
					"Malformed port: {0}. Using the default.", plugin
							.getProperties().get("remote-control-port"));
			port = 25561;
		}

		this.startChar = this.plugin.getConfig().getProperty("command-char",
				"@");

		this.plugin.log.info("Registered the chat listener!");

	}

	@EventHandler
	public void onChat(ChatEvent e) {

		if (e.getMessage().startsWith(this.startChar)
				&& e.getSender() instanceof ProxiedPlayer) {

			ProxiedPlayer player = (ProxiedPlayer) e.getSender();
			String[] args = (e.getMessage().startsWith("@") ? e.getMessage()
					.substring(1) : e.getMessage()).split(" ");
			
			if(doCommand(player, args)) e.setCancelled(true);;

		}

	}

	@SuppressWarnings("deprecation")
	public boolean doCommand(ProxiedPlayer player, String[] as) {

		if (as[0].equalsIgnoreCase("holdsrv")) {
			if (as.length != 2) {
				player.sendMessage((new StringBuilder()).append(ChatColor.RED)
						.append("Usage: ").append(this.startChar)
						.append("holdsrv <[username:]password>").toString());
				return true;
			}
			if (as[1].contains(":"))
				plugin.getProxy().getScheduler().runAsync(plugin, new UDPPacketSender((new StringBuilder()).append("hold:")
						.append(as[1].trim()).toString(), player, plugin, port));
			else
				plugin.getProxy().getScheduler().runAsync(plugin, new UDPPacketSender((new StringBuilder()).append("hold:")
						.append(player.getName()).append(":")
						.append(as[1].trim()).toString(), player, plugin, port));
			return true;
		}
		if (as[0].equalsIgnoreCase("restartproxy")) {
			if (as.length != 2) {
				player.sendMessage((new StringBuilder()).append(ChatColor.RED)
						.append("Usage: ").append(this.startChar)
						.append("restartsrv <[username:]password>")
						.toString());
				return true;
			}
			if (as[1].contains(":"))
				plugin.getProxy().getScheduler().runAsync(plugin, new UDPPacketSender((new StringBuilder()).append("restart:")
						.append(as[1].trim()).toString(), player, plugin, port));
			else
				plugin.getProxy().getScheduler().runAsync(plugin, new UDPPacketSender((new StringBuilder()).append("restart:")
						.append(player.getName()).append(":")
						.append(as[1].trim()).toString(), player, plugin, port));
			return true;
		}
		if (as[0].equalsIgnoreCase("reschedulerestart")) {
			if (as.length < 3) {
				player.sendMessage((new StringBuilder())
						.append(ChatColor.RED)
						.append("Usage: ")
						.append(this.startChar)
						.append("reschedulerestart <[username:]password> <time>")
						.toString());
				return true;
			}
			String s4 = "";
			for (int l = 2; l < as.length; l++)
				s4 = (new StringBuilder()).append(s4).append(as[2].trim())
						.append(" ").toString();

			s4 = s4.replaceAll(":", "-");
			if (as[1].contains(":"))
				plugin.getProxy().getScheduler().runAsync(plugin, new UDPPacketSender((new StringBuilder()).append("reschedule:")
						.append(s4.trim()).append(":").append(as[1].trim())
						.toString(), player, plugin, port));
			else
				plugin.getProxy().getScheduler().runAsync(plugin, new UDPPacketSender((new StringBuilder()).append("reschedule:")
						.append(s4.trim()).append(":").append(player.getName())
						.append(":").append(as[1].trim()).toString(), player,
						plugin, port));
			return true;
		}
		if (as[0].equalsIgnoreCase("stopwrapper")) {
			if (as.length != 2) {
				player.sendMessage((new StringBuilder()).append(ChatColor.RED)
						.append("Usage: ").append(this.startChar)
						.append("stopwrapper <[username:]password>")
						.toString());
				return true;
			}
			if (as[1].contains(":"))
				plugin.getProxy().getScheduler().runAsync(plugin, new UDPPacketSender(
						(new StringBuilder()).append("stopwrapper:")
								.append(as[1].trim()).toString(), player,
						plugin, port));
			else
				plugin.getProxy().getScheduler().runAsync(plugin, new UDPPacketSender((new StringBuilder())
						.append("stopwrapper:").append(player.getName())
						.append(":").append(as[1].trim()).toString(), player,
						plugin, port));
			return true;
		}
		return false;
	}

}
