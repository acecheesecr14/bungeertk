package eu.ac3_servers.dev.rtk.bungee;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.logging.Level;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public abstract class RTKCommand extends Command {

	protected RTKPlugin plugin;
	protected PrintStream out;
	protected PrintStream err;
	protected int port;

	public RTKCommand(RTKPlugin plugin, String name) {
		super(name);
		this.plugin = plugin;
		
		out = new PrintStream(new FileOutputStream(FileDescriptor.out));
		err = new PrintStream(new FileOutputStream(FileDescriptor.err));
		
		try {
			port = Integer.parseInt(((String) plugin.getProperties()
					.get("remote-control-port")).trim());
		} catch (Exception exception) {
			RTKPlugin.log.log(Level.INFO,
					"Malformed port: {0}. Using the default.",
					plugin.getProperties().get("remote-control-port"));
			port = 25561;
		}
		
		plugin.getLogger().info("Registered command: " + getName());
		
	}
	
	public RTKCommand(RTKPlugin plugin, String name, String permission, String... alias) {
		super(name, permission, alias);
		this.plugin = plugin;
	}
	
	@Override
	public void execute(CommandSender commandsender, String[] as) {

		if(commandsender instanceof ProxiedPlayer){
			StringBuilder sb = new StringBuilder(getName());
			if (as.length > 0) {
				for (String string : as) {
					sb.append(" ").append(string);
				}
			}
			((ProxiedPlayer) commandsender).chat(sb.toString());
			return;
		}
		
	}

}
