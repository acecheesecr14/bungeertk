package eu.ac3_servers.dev.rtk.bungee;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;

import com.google.common.io.ByteStreams;

import eu.ac3_servers.dev.rtk.bungee.commands.*;
import eu.ac3_servers.dev.rtk.bungee.listeners.DebugListener;
import eu.ac3_servers.dev.rtk.bungee.listeners.ChatListener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

public class RTKPlugin extends Plugin
{
	
    public String version;
    public static Logger log = null;
    private Properties properties;
    private Map<String, String> messageMap;
	private Properties config;
	private Properties properties_w;
	private static boolean debuging = false;

    public void onEnable()
    {
    	log = getLogger();
    	
    	try
    	{
    		File fileConfig = new File(getProxy().getPluginsFolder(), "rtk.properties");
    		if(!fileConfig.exists()){
    			
    			fileConfig.createNewFile();
                try (InputStream is = getResourceAsStream("rtk.properties");
                    OutputStream os = new FileOutputStream(fileConfig)) {
                    ByteStreams.copy(is, os);
                }
    			
    		}
    		config = loadProperties(new FileInputStream(fileConfig));
    		log.info("Debugging is :" + (debuging = Boolean.parseBoolean(config.getProperty("debugging"))));
    		
    	}catch(IOException e){
    		log.info("Error loading the plugin config!");
    	}
    	
        try
        {
            version = getDescription().getVersion();
            properties = loadProperties(new FileInputStream("toolkit/remote.properties"));
            properties_w = loadProperties(new FileInputStream("toolkit/wrapper.properties"));
            if((!properties_w.containsKey("stop-command") || !properties_w.getProperty("stop-command").equalsIgnoreCase("end")) && properties.getProperty("prevent-override", "false").equalsIgnoreCase("false")){
            	properties_w.setProperty("stop-command", "end");
            	properties_w.store(new FileOutputStream("toolkit/wrapper.properties"), "Overwriten by ace!");
            }
            messageMap = loadMap(new FileInputStream("toolkit/messages.txt"));
        }
        catch(IOException ioexception)
        {
            log.info((new StringBuilder()).append("Error loading Toolkit plugin properties: ").append(ioexception).toString());
            ioexception.printStackTrace();
        }
        
        registerCommands();
        
        long l = Runtime.getRuntime().totalMemory();
        long l1 = Runtime.getRuntime().maxMemory();
        System.out.println((new StringBuilder()).append("Memory max: ").append(l1).append(" bytes").toString());
        System.out.println((new StringBuilder()).append("Memory total: ").append(l).append(" bytes").toString());
        log.info((new StringBuilder()).append("Remote Toolkit Plugin ").append(version).append(" enabled!").toString());
    }

    private void registerCommands() {

    	PluginManager pm = getProxy().getPluginManager();
    	
    	if(isDebug()){
    		
    		pm.registerListener(this, new DebugListener(this));
    		
    	}
    	
    	pm.registerListener(this, new ChatListener(this));
    	
    	pm.registerCommand(this, new holdproxyCommand(this));
    	pm.registerCommand(this, new kickallCommand(this));
    	pm.registerCommand(this, new kickallholdCommand(this));
		pm.registerCommand(this, new kickallstopCommand(this));
		pm.registerCommand(this, new preschedulerestartCommand(this));
		pm.registerCommand(this, new restartproxyCommand(this));
		pm.registerCommand(this, new rtkunicodetestCommand(this));
		pm.registerCommand(this, new RTPINGCommand(this));
		pm.registerCommand(this, new sayCommand(this));
		//pm.registerCommand(this, new stopCommand(this));
		pm.registerCommand(this, new pstopwrapperCommand(this));
    	
	}

	public void onDisable()
    {
        log.info((new StringBuilder()).append("Remote Toolkit Plugin ").append(version).append(" disabled!").toString());
    }

    public Properties getProperties()
    {
        return properties;
    }

    public Map<String, String> getMessageMap()
    {
        return messageMap;
    }
    
        //TODO return eventHandler.onCommand(commandsender, command, s, as, this);

    private Properties loadProperties(InputStream inputstream)
        throws IOException
    {
        Properties properties1 = new Properties();
        properties1.load(inputstream);
        return properties1;
    }

    public static Map<String, String> loadMap(InputStream inputstream)
    {
        HashMap<String, String> hashmap;
        Scanner scanner;
        hashmap = new HashMap<String, String>();
        scanner = null;
        for(scanner = new Scanner(inputstream); scanner.hasNextLine();)
        {
            String as[] = scanner.nextLine().trim().split(":");
            String s = as[0];
            String s1 = "";
            for(int i = 1; i < as.length; i++)
                s1 = (new StringBuilder()).append(s1).append(":").append(as[i]).toString();

            if(s1.length() > 0)
                hashmap.put(s, s1.substring(1));
            else
                hashmap.put(s, "");
        }

        scanner.close();
        return hashmap;
    }
    
    public static boolean isDebug(){
    	
    	return debuging;
    	
    }
    
    public Properties getConfig(){
    	
    	return this.config;
    	
    }

}