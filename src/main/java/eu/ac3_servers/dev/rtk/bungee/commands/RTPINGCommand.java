package eu.ac3_servers.dev.rtk.bungee.commands;

import net.md_5.bungee.api.CommandSender;
import eu.ac3_servers.dev.rtk.bungee.RTKCommand;
import eu.ac3_servers.dev.rtk.bungee.RTKPlugin;

public class RTPINGCommand extends RTKCommand {

	public RTPINGCommand(RTKPlugin plugin) {
		super(plugin, "RTPING");
	}

	@Override
	public void execute(CommandSender commandsender, String[] as) {
		super.execute(commandsender, as);
		
		out.println("RTPONG++");
		
	}

}
