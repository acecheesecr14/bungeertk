package eu.ac3_servers.dev.rtk.bungee.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import eu.ac3_servers.dev.rtk.bungee.RTKCommand;
import eu.ac3_servers.dev.rtk.bungee.RTKPlugin;

public class kickallstopCommand extends RTKCommand {

	public kickallstopCommand(RTKPlugin plugin) {
		super(plugin, "kickallstop");
		// TODO Auto-generated constructor stub
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender commandsender, String[] as) {
		// TODO Auto-generated method stub
		super.execute(commandsender, as);
		
		String s3 = ((String) plugin.getMessageMap().get(
				"toolkit-shutdown-kick-message")).replaceAll(
				new String(new byte[] { 92, 92, 110 }), "\n");
		if (s3 == null)
			s3 = "Server is shutting down";
		for (ProxiedPlayer player : plugin.getProxy().getPlayers()) {
			player.disconnect(s3);
		}
	}

}
