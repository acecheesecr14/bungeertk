package eu.ac3_servers.dev.rtk.bungee.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import eu.ac3_servers.dev.rtk.bungee.RTKCommand;
import eu.ac3_servers.dev.rtk.bungee.RTKPlugin;

public class kickallCommand extends RTKCommand {

	public kickallCommand(RTKPlugin plugin) {
		super(plugin, "kickall");
	}

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender commandsender, String[] as) {

		super.execute(commandsender, as);
		
		String s1 = ((String) plugin.getMessageMap().get(
				"restart-kick-message")).replaceAll(new String(
				new byte[] { 92, 92, 110 }), "\n");
		if (s1 == null)
			s1 = "Everyone is being kicked ;)";
		for (ProxiedPlayer player : plugin.getProxy().getPlayers()) {
			player.disconnect(s1);
		}
		
	}

}
